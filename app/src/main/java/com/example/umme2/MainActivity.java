package com.example.umme2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.MenuPopupWindow;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
//import android.widget.PopupMenu;

import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
private ArrayList<model> shipdefectlist;
        ImageView img;
        Toolbar toolbar;
        final Context context = this;
        private ImageView button;


    /*  RecyclerView recyclerView;

      Context context;

      RecyclerView.Adapter recyclerView_Adapter;

      RecyclerView.LayoutManager recyclerViewLayoutManager;

      String[] numbers = {
              "HGB/001/19",
              "HGB/001/19",
              "HGB/001/19",
              "HGB/001/19",
              "three",
              "four",
              "five",
              "six",
              "seven",
              "eight",
              "nine",
              "ten",
              "eleven",

      };
      String[] department={
              "Radio",
              "Radio",
              "Radio",
              "Radio",
              "Radio",
              "Radio",

      };

*/

@Override
protected void onCreate(Bundle savedInstanceState){

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        button= (ImageView) findViewById(R.id.search_img);

        // add button listener
        button.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View arg0) {

                        // custom dialog
                        final Dialog dialog = new Dialog(context);
                        dialog.setContentView(R.layout.popuplayout);
                        dialog.setTitle("Title...");

                        // set the custom dialog components - text, image and button

                        Button dialogButton = (Button) dialog.findViewById(R.id.clear);
                        // if button is clicked, close the custom dialog
                        dialogButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                        dialog.dismiss();
                                }
                        });

                        dialog.show();
                }
        });

        toolbar=(Toolbar)findViewById(R.id.toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                        Toast.makeText(getApplicationContext(), "Back clicked!",     Toast.LENGTH_SHORT).show();
                }
        });
        Spinner spinner=(Spinner)findViewById(R.id.spinner);
        img=(ImageView)findViewById(R.id.search_img);
        img.setOnClickListener(new View.OnClickListener(){
@Override
public void onClick(View view){
final LayoutInflater inflater=(LayoutInflater)MainActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout=inflater.inflate(R.layout.popuplayout,null);
        PopupWindow window=new PopupWindow(layout,610,1250,true);
        window.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        window.setOutsideTouchable(true);
        window.showAtLocation(layout,Gravity.START,0,0);
        }
        });

        spinner.setPrompt("Username");
        List<String> cate=new ArrayList<String>();
        cate.add("item1");
        cate.add("item2");
        cate.add("item3");
        ArrayAdapter<String> data=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,cate);
        data.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(data);

        shipdefectlist=new ArrayList<model>();
        ListView lview=(ListView)findViewById(R.id.recyclerview1);
        listviewAdapter adapter=new listviewAdapter(this,shipdefectlist);
        lview.setAdapter(adapter);

        populateList();

        adapter.notifyDataSetChanged();

        lview.setOnItemClickListener(new AdapterView.OnItemClickListener(){

@Override
public void onItemClick(AdapterView<?> parent,View view,
        int position,long id){

        String shipdefectno=((TextView)view.findViewById(R.id.textview)).getText().toString();
        String description=((TextView)view.findViewById(R.id.tv1)).getText().toString();
        String department=((TextView)view.findViewById(R.id.tv2)).getText().toString();
        String priority=((TextView)view.findViewById(R.id.tv3)).getText().toString();
        String createdate=((TextView)view.findViewById(R.id.tv4)).getText().toString();
        String completedate=((TextView)view.findViewById(R.id.tv5)).getText().toString();
        Toast.makeText(getApplicationContext(),
        shipdefectno+"\n"
        +description+"\n"
        +department+"\n"
        +priority+"\n"
        +createdate+"\n"
        +completedate,Toast.LENGTH_SHORT).show();
        }
        });
        }


private void populateList(){

        model item1,item2,item3,item4,item5,item6,item7;

        item1=new model("HGB/001/19","SHORE SERVICE ORDERED SPARE ITEM PER V-15-TES-026-E spare mass flowmeters received, to be installed in the next DD, presently old gears of the FM overhauled and reused","Radio","Routine","11/02/2019","11/02/2019");
        shipdefectlist.add(item1);

        item2=new model("HGB/001/19","SHORE SERVICE ORDERED SPARE ITEM PER V-15-TES-026-E spare mass flowmeters received, to be installed in the next DD, presently old gears of the FM overhauled and reused","Radio","Routine","11/02/2019","11/02/2019");
        shipdefectlist.add(item2);

        item3=new model("HGB/001/19","SHORE SERVICE ORDERED SPARE ITEM PER V-15-TES-026-E spare mass flowmeters received, to be installed in the next DD, presently old gears of the FM overhauled and reused","Radio","Routine","11/02/2019","11/02/2019");
        shipdefectlist.add(item3);

        item4=new model("HGB/001/19","SHORE SERVICE ORDERED SPARE ITEM PER V-15-TES-026-E spare mass flowmeters received, to be installed in the next DD, presently old gears of the FM overhauled and reused","Radio","Routine","11/02/2019","11/02/2019");
        shipdefectlist.add(item4);

        item5=new model("HGB/001/19","SHORE SERVICE ORDERED SPARE ITEM PER V-15-TES-026-E spare mass flowmeters received, to be installed in the next DD, presently old gears of the FM overhauled and reused","Radio","Routine","11/02/2019","11/02/2019");
        shipdefectlist.add(item5);
        item6=new model("HGB/001/19","SHORE SERVICE ORDERED SPARE ITEM PER V-15-TES-026-E spare mass flowmeters received, to be installed in the next DD, presently old gears of the FM overhauled and reused","Radio","Routine","11/02/2019","11/02/2019");
        shipdefectlist.add(item6);
        item7=new model("HGB/001/19","SHORE SERVICE ORDERED SPARE ITEM PER V-15-TES-026-E spare mass flowmeters received, to be installed in the next DD, presently old gears of the FM overhauled and reused","Radio","Routine","11/02/2019","11/02/2019");
        shipdefectlist.add(item7);
        }


    /*   context = getApplicationContext();


       recyclerView = (RecyclerView) findViewById(R.id.recyclerview1);

       //Change 2 to your choice because here 2 is the number of Grid layout Columns in each row.
       recyclerViewLayoutManager = new GridLayoutManager(context, 1);

       recyclerView.setLayoutManager(recyclerViewLayoutManager);

       recyclerView_Adapter = new Recyclerview(context,numbers);

       recyclerView.setAdapter(recyclerView_Adapter);

   }*/
@Override
public void onItemSelected(AdapterView<?> parent,View view,int positon,long id){
        String item=parent.getItemAtPosition(positon).toString();
        }

@Override
public void onNothingSelected(AdapterView<?> arg0){

        }

/*@Override
public boolean onMenuItemClick(MenuItem item){
        Toast.makeText(this,"Selected Item: "+item.getTitle(),Toast.LENGTH_SHORT).show();
        switch(item.getItemId()){
        case R.id.search_item:
        // do your code
        return true;
        case R.id.upload_item:
        // do your code
        return true;
        case R.id.copy_item:
        // do your code
        return true;
        case R.id.print_item:
        // do your code
        return true;
        case R.id.share_item:
        // do your code
        return true;
        case R.id.bookmark_item:
        // do your code
        return true;
default:
        return false;
        }
        }*/


        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
                // Inflate the menu; this adds items to the action bar if it is present.
                getMenuInflater().inflate(R.menu.popupmenu, menu);
                return true;
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
                int id = item.getItemId();
                switch (id){
                        case R.id.upload_item:
                                Toast.makeText(getApplicationContext(),"Item 1 Selected",Toast.LENGTH_LONG).show();
                                return true;
                        case R.id.copy_item:
                                Toast.makeText(getApplicationContext(),"Item 2 Selected",Toast.LENGTH_LONG).show();
                                return true;
                        case R.id.search_item:
                                Toast.makeText(getApplicationContext(),"Item 3 Selected",Toast.LENGTH_LONG).show();
                                return true;
                        default:
                                return super.onOptionsItemSelected(item);
                }
        }

}