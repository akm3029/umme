package com.example.umme2;

import androidx.lifecycle.ViewModel;

public class model  {
    private String shipdefectno;
    private String description;
    private String department;
    private String priority;
    private String createdate;
    private String completedate;


    public model(String shipdefectno, String description, String department,  String priority, String createdate,String completedate) {
        this.shipdefectno = shipdefectno;
        this.description = description;
        this.department = department;
        this.priority=priority;
        this.createdate = createdate;
        this.completedate=completedate;
    }

    public String getShipdefectno() {
        return shipdefectno;
    }

    public String getDescription() {
        return description;
    }

    public String getDepartment() {
        return department;
    }

    public String getPriority() {
        return priority;
    }
    public String getCreatedate() {
        return createdate;
    }
    public String getCompletedate() {
        return completedate;
    }

}

