package com.example.umme2;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class listviewAdapter extends BaseAdapter {

        public ArrayList<model> shipdefectlist;
        Activity activity;

        public listviewAdapter(Activity activity, ArrayList<model> shipdefectlisr) {
            super();
            this.activity = activity;
            this.shipdefectlist= shipdefectlisr;
        }

        @Override
        public int getCount() {
            return shipdefectlist.size();
        }

        @Override
        public Object getItem(int position) {
            return shipdefectlist.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        private class ViewHolder {
            TextView shipdefectno;
            TextView description;
            TextView department;
            TextView priority;
            TextView createdate;
            TextView completedate;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder holder;
            LayoutInflater inflater = activity.getLayoutInflater();

            if (convertView == null) {
                convertView = inflater.inflate(R.layout.recyclerviewitem, null);
                holder = new ViewHolder();
                holder.shipdefectno = (TextView) convertView.findViewById(R.id.textview);
                holder.description = (TextView) convertView.findViewById(R.id.tv1);
                holder.department = (TextView) convertView.findViewById(R.id.tv2);
                holder.priority = (TextView) convertView.findViewById(R.id.tv3);
                holder.createdate = (TextView) convertView.findViewById(R.id.tv4);
                holder.completedate = (TextView) convertView.findViewById(R.id.tv5);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            model item = shipdefectlist.get(position);
            holder.shipdefectno.setText(item.getShipdefectno().toString());
            holder.description.setText(item.getDescription().toString());
            holder.department.setText(item.getDepartment().toString());
            holder.priority.setText(item.getPriority().toString());
            holder.createdate.setText(item.getCreatedate().toString());
            holder.completedate.setText(item.getCompletedate().toString());

            return convertView;
        }
}
